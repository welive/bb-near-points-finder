/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package es.deusto.morelab;

public class Route
        implements Comparable
{
	private String id;
    private double walkingSeconds;
    private double distanceMeters;
    private String destination;

    public Route()
    {
    }

    public Route(String id, double walkingSeconds, double distanceMeters, String destination)
    {
    	this.id = id;
        this.destination = destination;
        this.walkingSeconds = walkingSeconds;
        this.distanceMeters = distanceMeters;
    }

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getWalkingSeconds() {
        return this.walkingSeconds;
    }

    public void setWalkingSeconds(double walkingSeconds) {
        this.walkingSeconds = walkingSeconds;
    }

    public double getDistanceMeters() {
        return this.distanceMeters;
    }

    public void setDistanceMeters(double distanceMeters) {
        this.distanceMeters = distanceMeters;
    }

    public String getDestination() {
        return this.destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int compareTo(Object comparerou)
    {
        int comparedist = (int)((Route)comparerou).getDistanceMeters();

        return (int)this.distanceMeters - comparedist;
    }
}
