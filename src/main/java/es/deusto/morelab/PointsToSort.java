/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package es.deusto.morelab;

import java.util.List;

public class PointsToSort
{
    Double originLatitude;
    Double originLongitude;
    List<Coordinate> listOfCoordinates;

    public PointsToSort()
    {
    }

    public PointsToSort(Double originLatitude, Double originLongitude, List<Coordinate> listOfCoordinates)
    {
        this.originLatitude = originLatitude;
        this.originLongitude = originLongitude;
        this.listOfCoordinates = listOfCoordinates;
    }

    public Double getOriginLatitude() {
        return this.originLatitude;
    }

    public void setOriginLatitude(Double originLatitude) {
        this.originLatitude = originLatitude;
    }

    public Double getOriginLongitude() {
        return this.originLongitude;
    }

    public void setOriginLongitude(Double originLongitude) {
        this.originLongitude = originLongitude;
    }

    public List<Coordinate> getListOfCoordinates() {
        return this.listOfCoordinates;
    }

    public void setListOfCoordinates(List<Coordinate> listOfCoordinates) {
        this.listOfCoordinates = listOfCoordinates;
    }
}